import { FakeTranslateEngine } from '../../src/translatorEngines/FakeTranslateEngine';

describe('FakeTranslateEngine', () => {
    let translator: FakeTranslateEngine;

    beforeEach(() => {
        translator = new FakeTranslateEngine();
    });

    test('should return translated text if translation exists in the dictionary', async () => {
        const result = await translator.translate('hello', 'fr');
        expect(result).toBe('bonjour');
    });

    test('should return the original text if translation does not exist in the dictionary', async () => {
        const result = await translator.translate('example', 'fr');
        expect(result).toBe('example');
    });

    test('should return translated text for different target locale', async () => {
        const result = await translator.translate('hello', 'es');
        expect(result).toBe('hola');
    });

    test('should return the original text for different target locale if translation does not exist', async () => {
        const result = await translator.translate('example', 'es');
        expect(result).toBe('example');
    });

});
