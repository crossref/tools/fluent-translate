import { StringObject } from './../../src/types';
// test/unit/FtlFileParser.spec.ts
import { FtlFileParser } from '../../src/fileParsers/FtlFileParser';
import { parse as nativeFluentParse, Resource as FluentResource } from '@fluent/syntax';

describe('FtlFileParser', () => {
    let parser: FtlFileParser;

    beforeEach(() => {
        parser = new FtlFileParser();
    });

    test('should correctly parse and serialize Fluent files', () => {
        const content = 
`hello = world

ror_matched_institutions =
  { $count ->
    [one] Matched one institution
   *[other] Matched { $count } institutions
  }

`;
         const nativeParsed: FluentResource = nativeFluentParse(content, {})
        const flatObject: StringObject = parser.parse(content);
        const reconstituedFluentString = parser.serialize(flatObject)
        expect(content).toEqual(reconstituedFluentString)


        // For testing, we use Fluent's parse function to compare
        // expect(nativeParsed).toEqual(nativeFluentParse(reconstituedFluentString, {}))
        // expect(parsed).toEqual(parse(content, {}));

        // const serialized = parser.serialize(parsed);
        // expect(serialized.trim()).toBe(content.trim());
    });

    });
