import { TranslationService } from '../../src/TranslationService';
import { FakeTranslateEngine } from '../../src/translatorEngines/FakeTranslateEngine';

describe('TranslationService', () => {
    let translationService: TranslationService;
    let fakeTranslateEngine: FakeTranslateEngine;

    beforeEach(() => {
        fakeTranslateEngine = new FakeTranslateEngine();
        translationService = new TranslationService(fakeTranslateEngine);
    });

    test('should correctly translate strings', async () => {
        const sourceText = 'hello';
        const targetLocale = 'fr';
        const expectedTranslation = 'bonjour';
        const translation = await translationService.translate(sourceText, targetLocale);

        expect(translation).toBe(expectedTranslation);
    });

    });
