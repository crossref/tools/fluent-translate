// test/unit/FakeFileLoader.spec.ts
import { FakeFileLoader } from '../../src/fileLoaders/FakeFileLoader';

describe('FakeFileLoader', () => {
    let loader: FakeFileLoader;

    beforeEach(() => {
        loader = new FakeFileLoader('Hello, world!');
    });

    test('should return hardcoded data when loading', async () => {
        const result = await loader.load('any/path');
        expect(result).toBe('Hello, world!');
    });

    test('should do nothing when writing', async () => {
        await expect(loader.write('any/path', 'any content')).resolves.toBeUndefined();
    });
});
