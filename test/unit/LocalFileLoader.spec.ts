// test/unit/LocalFileLoader.spec.ts
import { LocalFileLoader } from '../../src/fileLoaders/LocalFileLoader';
import mockFs from 'mock-fs';

describe('LocalFileLoader', () => {
    let loader: LocalFileLoader;

    beforeEach(() => {
        loader = new LocalFileLoader();
        // Set up a mock filesystem
        mockFs({
            'test.txt': 'Hello, world!'
        });
    });

    afterEach(() => {
        // Restore the filesystem after each test
        mockFs.restore();
    });

    test('should return file content when loading', async () => {
        const result = await loader.load('test.txt');
        expect(result).toBe('Hello, world!');
    });

    test('should write content to file', async () => {
        const content = 'This is a test';
        await loader.write('output.txt', content);
        const result = await loader.load('output.txt');
        expect(result).toBe(content);
    });

    });
