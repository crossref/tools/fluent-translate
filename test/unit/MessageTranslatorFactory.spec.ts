import { MessageTranslatorFactory } from '../../src/messageTranslators/MessageTranslatorFactory';
import { FluentMessageTranslator } from '../../src/messageTranslators/FluentMessageTranslator';

describe('MessageTranslatorFactory', () => {
    let factory: MessageTranslatorFactory;

    beforeEach(() => {
        factory = new MessageTranslatorFactory();
    });

    it('should create FluentMessageTranslator for FTL files', () => {
        const translator = factory.createMessageTranslator('file.ftl');

        expect(translator).toBeInstanceOf(FluentMessageTranslator);
    });

    // Additional tests can be added here...
});
