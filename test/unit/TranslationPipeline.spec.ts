import * as fs from 'fs';
import { TranslationPipeline } from '../../src/TranslationPipeline';
import { FileParserFactory } from '../../src/fileParsers/FileParserFactory';
import { FtlFileParser } from '../../src/fileParsers/FtlFileParser';
import { TranslationService } from '../../src/TranslationService';
import { FakeTranslateEngine } from '../../src/translatorEngines/FakeTranslateEngine';
import { MessageTranslatorFactory } from '../../src/messageTranslators/MessageTranslatorFactory';

jest.mock('fs', () => ({
  ...jest.requireActual('fs'),
  readFileSync: jest.fn(),
  promises: {
    writeFile: jest.fn().mockResolvedValue(undefined),
  },
}));


describe('TranslationPipeline', () => {
  let pipeline: TranslationPipeline;
  let fakeTranslator: FakeTranslateEngine;
  let ftlFileParser: FtlFileParser;
  let translationService: TranslationService;
  let mockFactory: FileParserFactory;
  let messageTranslatorFactory: MessageTranslatorFactory

  beforeEach(() => {
      fakeTranslator = new FakeTranslateEngine();
      translationService = new TranslationService(fakeTranslator);
      ftlFileParser = new FtlFileParser();
      mockFactory = { createParser: jest.fn().mockReturnValue(ftlFileParser) }; // Mock factory
      messageTranslatorFactory = new MessageTranslatorFactory(translationService)
      pipeline = new TranslationPipeline(mockFactory, translationService, messageTranslatorFactory, './output');
  });

        const sourceContent = 
`hello = world
`;
    
    test('should correctly translate file content', async () => {
      (fs.readFileSync as jest.Mock).mockReturnValue(sourceContent);

        const sourceFile = './path-to-your-file.ftl';
        const targetLocales = ['fr', 'es'];
        const expectedTranslations = {
            'fr': {
                'hello': 'monde'
            },
            'es': {
                'hello': 'mundo'
            }
        };

        const translations = await pipeline.translateFile(sourceFile, targetLocales);
        expect(translations).toEqual(expectedTranslations);
        expect(fs.promises.writeFile).toHaveBeenCalledTimes(2); // We expect 2 calls because we have 2 target locales

    });

    afterEach(() => {
        jest.resetAllMocks();
    });
});
