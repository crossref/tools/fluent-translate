import { GoogleTranslateEngine } from '../../src/translatorEngines/GoogleTranslateEngine';
import { TranslationServiceClient } from '@google-cloud/translate';


describe('GoogleTranslateEngine', () => {
  let engine: GoogleTranslateEngine;
  let translateTextSpy: jest.SpyInstance;

  beforeEach(() => {
      engine = new GoogleTranslateEngine();

      // Mock the translateText method
      translateTextSpy = jest.spyOn(TranslationServiceClient.prototype, 'translateText');
  });

  afterEach(() => {
      // Clear all mocks after each test
      jest.clearAllMocks();
  });

  it('should preserve trailing whitespace during translation', async () => {
      const text = 'Hello, world!  '; // two spaces at the end
      const translatedTextWithoutSpaces = 'Bonjour le monde!';

      translateTextSpy.mockResolvedValue([
          {
              translations: [{ translatedText: translatedTextWithoutSpaces }]
          }
      ]);

      const translatedText = await engine.translate(text, 'fr');

      // Assuming the translation is correct, check if the trailing whitespace is preserved
      expect(translatedText).toMatch(/  $/); // two spaces at the end
  });
});