// test/unit/FileLoaderService.spec.ts
import { FileLoaderService } from '../../src/FileLoaderService';
import { FileLoaderInterface } from '../../src/interfaces/FileLoaderInterface';

// A mock FileLoader for use in our tests
class MockFileLoader implements FileLoaderInterface {
    load(filePath: string): Promise<string> {
        return Promise.resolve('Mock file content');
    }
    
    write(filePath: string, content: string): Promise<void> {
        return Promise.resolve();
    }
}

describe('FileLoaderService', () => {
    let service: FileLoaderService;
    let mockLoader: FileLoaderInterface;

    beforeEach(() => {
        mockLoader = new MockFileLoader();
        service = new FileLoaderService(mockLoader);
    });

    test('should correctly utilize a file loader that implements the FileLoaderInterface', async () => {
        const result = await service.loadFile('path/to/mock/file.txt');
        expect(result).toBe('Mock file content');
    });

    });
