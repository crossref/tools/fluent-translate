// test/integration/TranslationPipeline.spec.ts
import fs from 'fs';
import path from 'path';
import { TranslationPipeline } from '../../src/TranslationPipeline';
import { FileParserFactory } from '../../src/fileParsers/FileParserFactory';
import { TranslationService } from '../../src/TranslationService';
import { GoogleTranslateEngine } from '../../src/translatorEngines/GoogleTranslateEngine';
import { MessageTranslatorFactory } from '../../src/messageTranslators/MessageTranslatorFactory';

jest.mock('fs', () => ({
  ...jest.requireActual('fs'),
  readFileSync: jest.fn(),
  promises: {
    writeFile: jest.fn().mockResolvedValue(undefined),
  },
}));

describe('TranslationPipeline', () => {
  let pipeline: TranslationPipeline;
  let googleTranslatorEngine: GoogleTranslateEngine;
  let translationService: TranslationService;
  let fileParserFactory: FileParserFactory;
  let messageTranslatorFactory: MessageTranslatorFactory

  beforeEach(() => {
    googleTranslatorEngine = new GoogleTranslateEngine();
    translationService = new TranslationService(googleTranslatorEngine);
    fileParserFactory = new FileParserFactory();
    messageTranslatorFactory = new MessageTranslatorFactory(translationService)
    pipeline = new TranslationPipeline(fileParserFactory, translationService, messageTranslatorFactory, './output');

    // Mock the implementation of GoogleTranslatorEngine's translate function
    jest.spyOn(googleTranslatorEngine, 'translate').mockImplementation((text, targetLocale) => {
      if (targetLocale === 'fr') return Promise.resolve('bonjour');
      if (targetLocale === 'es') return Promise.resolve('hola');
      return Promise.resolve(text);
    });

    // Mock the implementation of fs.readFileSync
    (fs.readFileSync as jest.Mock).mockImplementation((filePath) => {
      if (path.basename(filePath.toString()) === 'hello.ftl') {
        return 'hello = world';
      }
      return '';
    });
  });

  it('should correctly translate file content', async () => {
    const sourceFile = './hello.ftl';
    const targetLocales = ['fr', 'es'];
    const expectedTranslations = {
      fr: {
        hello: 'bonjour',
      },
      es: {
        hello: 'hola',
      },
    };

    const translations = await pipeline.translateFile(sourceFile, targetLocales);

    expect(translations).toEqual(expectedTranslations);
    expect(fs.promises.writeFile).toHaveBeenCalledTimes(2); // We expect writeFile to be called twice (one for each locale)
  });
});
