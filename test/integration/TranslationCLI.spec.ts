import { spawn } from 'child_process';
import * as fs from 'fs';
import * as path from 'path';

describe('Translation CLI', () => {
  it('translates text correctly', (done) => {
    const inputFilePath = path.join(__dirname, './app.ftl');
    const outputDirPath = path.join(__dirname, '../../output/');
    const outputFilePath = path.join(__dirname, '../../output/es/app.ftl');
    const scriptFilePath = path.join(__dirname, '../../dist/cli.js')
    const translationProcess = spawn('node', [
      scriptFilePath,
      '-i',
      inputFilePath,
      '-o',
      outputDirPath,
      '--to',
      'es',
      '--engine',
      'Fake'
    ]);
    let stdout = "", stderr = "";

    translationProcess.stdout.on('data', (data) => {
  stdout += data;
});

translationProcess.stderr.on('data', (data) => {
  stderr += data;
});

    translationProcess.on('close', (code) => {
      if (code !== 0) {
        console.log(`STDOUT: ${stdout}`);
        console.log(`STDERR: ${stderr}`);
        done(new Error(`Translation process exited with code ${code}`));
        return;
      }
      const translatedText = fs.readFileSync(outputFilePath, 'utf-8');
      expect(translatedText).toEqual('hello = mundo\n\n');
      done();
    });
  });
});
