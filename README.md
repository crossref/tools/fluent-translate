<h1 align="center">Fluent Translate</h1>

The command-line interface (CLI) for automated translation of Fluent .ftl message files.

### Features
- Automated translation for .ftl files
- Built-in support for Google Translate
- Multiple target languages
- Locale-specific output directories

<p align="center">
  <i>Support this project by starring and sharing it. Every contribution is appreciated!</i>
</p>

  ## Installation

  You can install Fluent Translate globally by running:

  ```bash
  npm install -g fluent-translate
  ```

  This will make the `fluent-translate` command available globally on your system.

## About

Fluent Translate automates the process of translating Fluent .ftl message files. It uses Google Translate for accurate translations, and supports multiple target languages. The translated .ftl files are saved in locale-specific directories, organized and ready for use.

To translate an .ftl file:

`fluent-translate -i /path/to/my/input/file.ftl -o /my/output/dir --to fr,id --engine Google`

  ## Google Cloud Setup

  For Google Translate, you need to set up a project in Google Cloud, enable the Cloud Translation API, and create a service account. Then, download the JSON key file for the service account.

  Set the `GOOGLE_APPLICATION_CREDENTIALS` environment variable in your system to the path of the service account key file. For example

  `export GOOGLE_APPLICATION_CREDENTIALS="/home/user/Downloads/service-account-file.json"`

  Also, set the `GOOGLE_CLOUD_PROJECT_ID` environment variable in your system to your Google Cloud project ID. For example:

  `export GOOGLE_CLOUD_PROJECT_ID="my-project-id"`

  Remember to replace the paths and IDs in the above examples with the ones from your own setup.

  ## Usage

  `fluent-translate -i <inputFile> -o <outputDir> [options]`

  `<inputFile>`: The path to the .ftl file you want to translate.

  `<outputDir>`: The directory where the translated .ftl files will be saved. Each locale will be saved in a different subdirectory, for example:

  `fluent-transate /path/to/my/input/file.ftl /my/output/dir --to fr,id`

  will result in:

  ```
  /my/output/dir/
  ├── fr/
  │   └── file.ftl
  └── id/
      └── file.ftl
  ```

  Options:

  - `--from <language>`: Source language (default: 'en-GB')
  - `--to <language>`: Target language(s), comma separated (default: 'es')
  - `--engine <engine>`: Translation engine (default: 'Google')

  ## Contributing

Contributions to `fluent-translate` are welcome. Fork the repository, make your changes, and submit a Pull Request. For larger changes, please open an issue to discuss your plans first.

  ## License

  This project is licensed under the ISC license.
