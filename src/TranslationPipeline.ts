import { parse, stringify } from 'fluent-translation-parser';
import js2ftl from 'fluent_conv/cjs/js2ftl';
import * as fs from 'fs';
import * as path from 'path';
import { FileParserFactory } from './fileParsers/FileParserFactory';
import { TranslationService } from './TranslationService';
import { StringObject } from './types';
import { MessageTranslatorFactory } from './messageTranslators/MessageTranslatorFactory';

export class TranslationPipeline {
    constructor(
        private fileParserFactory: FileParserFactory,
        private translationService: TranslationService,
        private messageTranslatorFactory: MessageTranslatorFactory,
        private outputDir: string
    ) {}

    async translateFile(file: string, targetLocales: string[], sourceLocale: string='en-GB', ): Promise<{[locale: string]: StringObject }> {
        const fileContent = this.readFile(file);
        const parsedContent = this.parseFile(file)
        return this.translateContent(file, parsedContent, targetLocales);
    }

    private readFile(file: string): string {
        return fs.readFileSync(file, 'utf-8');
    }

    private parseFile(file: string): StringObject {
        const content = this.readFile(file)
        const fileParser = this.fileParserFactory.createParser(file);
        const parsedContent = fileParser.parse(content)
        return parsedContent
    }

    private writeFile(sourceFilePath: string, locale: string, content: StringObject): Promise<void> {
        const fileName = sourceFilePath.split('/').pop() || 'output.ftl'; // Defaults to 'output.ftl' if the sourceFilePath doesn't have a file name
        const outputFilePath = path.join(this.outputDir, locale, fileName);
        // Ensure the output directory exists before writing the file
        this.ensureDirExists(path.dirname(outputFilePath));

        return fs.promises.writeFile(outputFilePath, js2ftl(content)); // JSON.stringify(content, null, 2) is used for pretty printing. Replace with your serialization logic if needed.
    }

    private async translateContent(sourceFilePath: string, keyValuePairs: StringObject, targetLocales: string[]): Promise<{[locale: string]: StringObject }> {
        let translatedFiles: { [key: string]: StringObject} = {};
        for (const locale of targetLocales) {
            let translatedContent: { [key: string]: string } = {};

            const messageTranslator = this.messageTranslatorFactory.createMessageTranslator(sourceFilePath);
            for (const key in keyValuePairs) {
                const text = keyValuePairs[key];
                const translatedText = await messageTranslator.translate(text, locale, this.translationService);
                translatedContent[key] = translatedText;
            }
            translatedFiles[locale] = translatedContent;

            await this.writeFile(sourceFilePath, locale, translatedContent);
        }

        return translatedFiles;
    }

    private ensureDirExists(dirPath: string) {
        if (!fs.existsSync(dirPath)) {
            fs.mkdirSync(dirPath, { recursive: true });
        }
    }
}
