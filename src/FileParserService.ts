// src/FileParserService.ts
import { FileParserInterface } from './interfaces/FileParserInterface';

export class FileParserService {
    constructor(private parser: FileParserInterface) {}

    parse(fileContent: string): { [key: string]: string } {
        return this.parser.parse(fileContent);
    }

    serialize(contentObj: { [key: string]: string }): string {
        return this.parser.serialize(contentObj);
    }
}
