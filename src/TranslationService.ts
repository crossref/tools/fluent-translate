import { TranslatorInterface } from './interfaces/TranslatorInterface';

export class TranslationService {
    private translator: TranslatorInterface;

    constructor(translator: TranslatorInterface) {
        this.translator = translator;
    }

    /**
     * Translates a text string into the target locale.
     *
     * @param text - The text string to translate.
     * @param targetLocale - The target locale to translate the text into.
     * @return The translated text.
     */
    async translate(text: string, targetLocale: string): Promise<string> {
        return this.translator.translate(text, targetLocale);
    }
}