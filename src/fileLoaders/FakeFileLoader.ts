// src/fileLoaders/FakeFileLoader.ts
import { FileLoaderInterface } from '../interfaces/FileLoaderInterface';

export class FakeFileLoader implements FileLoaderInterface {
    private fakeData: string;

    constructor(fakeData: string) {
        this.fakeData = fakeData;
    }

    async load(filePath: string): Promise<string> {
        return this.fakeData;
    }

    async write(filePath: string, content: string): Promise<void> {
        // Do nothing in this fake implementation.
    }
}
