// src/fileLoaders/LocalFileLoader.ts
import { FileLoaderInterface } from '../interfaces/FileLoaderInterface';
import fs from 'fs/promises';

export class LocalFileLoader implements FileLoaderInterface {
    async load(filePath: string): Promise<string> {

        const content = await fs.readFile(filePath, 'utf-8');
        return content;
    }

    async write(filePath: string, content: string): Promise<void> {
        await fs.writeFile(filePath, content);
    }
}
