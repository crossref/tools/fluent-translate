import { FluentMessageTranslator } from './FluentMessageTranslator';

export class MessageTranslatorFactory {
    constructor() {}

    createMessageTranslator(file: string) {
        const fileExtension = file.split('.').pop();
        switch (fileExtension) {
            case 'ftl':
                return new FluentMessageTranslator();
            default:
                throw new Error(`Unsupported file type: ${fileExtension}`);
        }
    }
}
