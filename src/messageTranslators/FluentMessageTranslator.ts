import { MessageTranslatorInterface } from "../interfaces/MessageTranslatorInterface";
import { TranslationService } from "../TranslationService";
import { ASTNode, parse, stringify } from "fluent-translation-parser";

export class FluentMessageTranslator implements MessageTranslatorInterface {
    private async translateNode(node: ASTNode, translationService: TranslationService, targetLocale: string): Promise<ASTNode> {
        // Translate children nodes if available
        if (node.children) {
            for (let i = 0; i < node.children.length; i++) {
                node.children[i] = await this.translateNode(node.children[i], translationService, targetLocale);
            }
            node.content = this.stringify(node.children);
        }
    
        // Translate content of the node if it doesn't have children
        if (!node.children || node.children.length === 0) {
            if (node.type === 'text' && node.content?.trim() !== '' && typeof node.content === 'string' && node.content.length > 0 )
            node.content = await translationService.translate(node.content, targetLocale);
        }
    
        return node;
    }

    async translate(message: string, targetLocale: string, translationService: TranslationService): Promise<string> {
        const ast = parse(message);
    
        for (let i = 0; i < ast.length; i++) {
            ast[i] = await this.translateNode(ast[i], translationService, targetLocale);
        }
    
        return stringify(ast);
    }

      private stringify(ast: ASTNode[]): string {
        let str = '';
    
        for (const node of ast) {
            switch (node.type) {
                case 'text':
                    str += node.content;
                    break;
                case 'variable':
                    str += `{${node.content}}`;
                    break;
                case 'reference':
                    str += `{${node.content}}`;
                    break;
                case 'selector':
                    str += `{${node.content}->`;
                    break;
                case 'variant':
                    if (node.isDefault) {
                        str += `*[${node.content}]`;
                    } else {
                        str += `[${node.content}]`;
                    }
                    break;
                case 'openingBracket':
                    str += '{';
                    break;
                case 'closingBracket':
                    str += '}';
                    break;
                default:
                    if (node.children) {
                        str += stringify(node.children);
                    }
                    break;
            }
        }
    
        return str;
    }
    
}
