#!/usr/bin/env node

import yargs from "yargs/yargs";
import { hideBin } from "yargs/helpers";
import chalk from "chalk";
import ora from "ora";
import { TranslationService } from "./TranslationService";
import { FakeTranslateEngine } from "./translatorEngines/FakeTranslateEngine";
import { GoogleTranslateEngine } from "./translatorEngines/GoogleTranslateEngine";
import { TranslationPipeline } from "./TranslationPipeline";
import { FileParserFactory } from "./fileParsers/FileParserFactory";
import { MessageTranslatorFactory } from "./messageTranslators/MessageTranslatorFactory";
import { debug } from "console";

async function main() {
  const argv = await yargs(hideBin(process.argv))
    .option("inputFile", {
      alias: "i",
      description: "Input file to translate",
      type: "string",
      requiresArg: true,
    })
    .option("outputDir", {
      alias: "o",
      description: "Directory to output translated files",
      type: "string",
      requiresArg: true,
    })
    .option("from", {
      alias: "f",
      description: "Source language",
      type: "string",
      default: "en-GB",
    })
    .option("to", {
      alias: "t",
      description: "Target languages",
      type: "string",
      default: "es",
    })
    .option("engine", {
      alias: "e",
      description: "Translation engine",
      type: "string",
      default: "Google",
    })
    .help()
    .alias("help", "h").argv;

  const inputFile = argv.inputFile;
  const outputDir = argv.outputDir;

  if (!inputFile || !outputDir) {
    console.error(chalk.red("Both inputFile and outputDir must be specified."));
    process.exit(1);
  }

  let spinner = ora("Starting translation process...").start();

  try {
    const sourceLang = argv.from;
    const toLang = argv.to.split(",");
    let translateEngine;
    switch (argv.engine) {
      case "Google":
        translateEngine = new GoogleTranslateEngine();
        break;
      case "Fake":
        translateEngine = new FakeTranslateEngine();
        break;
      // case 'DeepL':
      //     translateEngine = new DeepLEngine();
      //     break;
      default:
        throw new Error(`Invalid translation engine: "${argv.engine}"`);
    }
    const translationService = new TranslationService(translateEngine);
    const fileParserFactory = new FileParserFactory();
    const messageTranslatorFactory = new MessageTranslatorFactory();
    const pipeline = new TranslationPipeline(
      fileParserFactory,
      translationService,
      messageTranslatorFactory,
      outputDir
    );
    const translations = pipeline.translateFile(inputFile, toLang, sourceLang);
    spinner.text = "Translating file...";
    await pipeline.translateFile(inputFile, toLang, sourceLang);

    spinner.succeed(chalk.green("Translation process completed successfully!"));
  } catch (error) {
    const err = error as Error;
    spinner.fail(chalk.red(`Translation process failed: ${err.message}`));
  }
}

main();
