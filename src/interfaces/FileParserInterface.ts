// src/interfaces/FileParserInterface.ts
export interface FileParserInterface {
  parse(fileContent: string): any;
  toKeyValue(parsedContent: any): { [key: string]: string; };
  serialize(contentObj: { [key: string]: string; }): string;
}
