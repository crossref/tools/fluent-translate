// src/interfaces/FileLoaderInterface.ts
export interface FileLoaderInterface {
  load(filePath: string): Promise<string>;
  write(filePath: string, content: string): Promise<void>;
}
