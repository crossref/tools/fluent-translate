import { StringObject } from './../types';
// interfaces/TranslationPipelineInterface.ts
export interface TranslationPipelineInterface {
  translate(
      sourceContent: StringObject,
      targetLocale: string
  ): Promise<StringObject>;
}
