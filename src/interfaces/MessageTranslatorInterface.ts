import { TranslationService } from "../TranslationService";

export interface MessageTranslatorInterface {
  /**
   * Translates a text string into the target locale.
   *
   * @param text - The text string to translate.
   * @param targetLocale - The target locale to translate the text into.
   * @param translationService - The translation service to use for translating the text.
   * @return The translated text.
   */
  translate(text: string, targetLocale: string, translationService: TranslationService): Promise<string>;
}
