/**
 * TranslatorInterface.ts
 */

export interface TranslatorInterface {
    /**
     * Translates a text string into the target locale.
     *
     * @param text - The text string to translate.
     * @param targetLocale - The target locale to translate the text into.
     * @return The translated text.
     */
    translate(text: string, targetLocale: string): Promise<string>;
}
