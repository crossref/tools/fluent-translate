// src/FileLoaderService.ts
import { FileLoaderInterface } from './interfaces/FileLoaderInterface';

export class FileLoaderService {
    private loader: FileLoaderInterface;

    constructor(loader: FileLoaderInterface) {
        this.loader = loader;
    }

    async loadFile(filePath: string): Promise<string> {
        return this.loader.load(filePath);
    }

    async writeFile(filePath: string, content: string): Promise<void> {
        return this.loader.write(filePath, content);
    }
}
