// src/fileParsers/FileParserFactory.ts
import { FileParserInterface } from "../interfaces/FileParserInterface";
import { FtlFileParser } from "./FtlFileParser";

export class FileParserFactory {
  createParser(filePath: string): FileParserInterface {
    const fileExtension = filePath.split('.').pop();

    switch (fileExtension) {
      case 'ftl':
        return new FtlFileParser();
      default:
        throw new Error(`No parser available for files with extension .${fileExtension}`);
    }
  }
}
