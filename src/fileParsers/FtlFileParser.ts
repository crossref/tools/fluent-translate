import { FileParserInterface } from '../interfaces/FileParserInterface';
import { parse, Resource, Entry, Message, PatternElement, TextElement } from '@fluent/syntax';
import ftl2js from 'fluent_conv/cjs/ftl2js'
import js2ftl from 'fluent_conv/cjs/js2ftl'
import { StringObject } from '../types';

function extractValue(elements: PatternElement[]): string {
    const textElement = elements.find(element => element instanceof TextElement) as TextElement;
    return textElement ? textElement.value : '';
}


export class FtlFileParser implements FileParserInterface {
    parse(fileContent: string): StringObject {
        return ftl2js(fileContent);
    }

    toFlatFormat(fileContent: string) {
        return ftl2js(fileContent)
    }

    toKeyValue(fileContent: string): { [key: string]: any } {
        return this.toFlatFormat(fileContent)
        const parsedFile: Resource = parse(fileContent, {});

        return parsedFile;
    }

    serialize(contentObj: { [key: string]: string; }): string {
        // Convert the key-value object to an FTL string
        return js2ftl(contentObj)
    }
}
