/**
 * GoogleTranslateEngine.ts
 */

import { TranslatorInterface } from "../../src/interfaces/TranslatorInterface";
import { TranslationServiceClient } from "@google-cloud/translate";

export class GoogleTranslateEngine implements TranslatorInterface {
  private client: TranslationServiceClient;

  constructor() {
    // Set up the client using configuration variables
    this.client = new TranslationServiceClient();
  }

  async translate(text: string, targetLocale: string): Promise<string> {
    const request = {
      parent: `projects/${process.env.GOOGLE_CLOUD_PROJECT_ID}/locations/global`,
      contents: [text],
      mimeType: "text/plain",
      targetLanguageCode: targetLocale,
    };

    const [response] = await this.client.translateText(request);

    let translatedText = response.translations?.[0]?.translatedText || "";

    // Identify the trailing whitespaces from the original text
    const originalTrailingWhitespace = text.match(/\s*$/)![0];

    // Identify the trailing whitespaces from the translated text
    const translatedTrailingWhitespace = translatedText.match(/\s*$/)![0];

    // If the translated text has fewer trailing whitespaces, add the missing ones
    if (
      originalTrailingWhitespace.length > translatedTrailingWhitespace.length
    ) {
      const missingWhitespace = originalTrailingWhitespace.slice(
        translatedTrailingWhitespace.length
      );
      translatedText += missingWhitespace;
    }

    // If the translated text has more trailing whitespaces, remove the extra ones
    else if (
      translatedTrailingWhitespace.length > originalTrailingWhitespace.length
    ) {
      translatedText = translatedText.slice(
        0,
        translatedText.length -
          (translatedTrailingWhitespace.length -
            originalTrailingWhitespace.length)
      );
    }

    return translatedText;
  }
}
