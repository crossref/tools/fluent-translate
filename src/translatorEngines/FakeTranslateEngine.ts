import { TranslatorInterface } from "../interfaces/TranslatorInterface";

export class FakeTranslateEngine implements TranslatorInterface {
    private dictionary: { [locale: string]: { [text: string]: string } };

    constructor() {
        this.dictionary = {
            'fr': {
                'hello': 'bonjour',
                'world': 'monde'
            },
            'es': {
                'hello': 'hola',
                'world': 'mundo'
            },
            'de': {
                'hello': 'hallo',
                'world': 'Welt'
            },
            'id': {
                'hello': 'halo',
                'world': 'Dunia'
            }
            // ... more dictionaries as needed
        };
    }

    async translate(text: string, targetLocale: string): Promise<string> {
        return this.dictionary[targetLocale]?.[text] || text;
    }
}
